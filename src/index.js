  $(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 700);
          return false;
        }
      }
    });
  });

function showLightbox() {
    document.getElementById('over').style.display='block';
    document.getElementById('fade').style.display='block';
}
function hideLightbox() {
    document.getElementById('over').style.display='none';
    document.getElementById('fade').style.display='none';
};

$(document).ready(function(){
  $('#aMenu').click(function(){
    $('#navmenu').toggleClass('nav-menu-list--show')
    });
  });

$(document).ready(function (){
  $('#aHome').click(function(){
    $('#HomeOver').toggleClass('home-container-over--show')
    $('#HomeOver1').toggleClass('home-container-text1--show')
    $('#HomeOver2').toggleClass('home-container-text2--show')
    });
  });